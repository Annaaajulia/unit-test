//TODO: arithmetic operations
/**
 * Adds two numbers together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * Divides two number
 * @param {number} divivdend 
 * @param {number} divisor 
 * @returns {number}     
 * @throws {Error} 0 division                   
 */

const divide = (divivdend, divisor) => {
    if (divisor == 0) throw new Error ("0 division not allowed");
    const fraction  = divivdend/divisor;
    return fraction;
};

export default {add, subtract, multiply, divide}
