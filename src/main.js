import express from 'express';
import calc from './calc.js';

const app = express();
const port = 3001;
const host = "localhost";

// ENDPOINT - GET http://localhost:3001/

app.get('/', (req, res) => {
    res.status(200).send("Hello world!");
});

// ENDPOINT - GET http://localhost:3001/add?a=2&b=3

app.get('/add', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const sum = calc.add(a, b);
    res.status(200).send(sum.toString());
});

app.listen(port, host, () => {
    console.log(`http://${host}:${port}`)
});
